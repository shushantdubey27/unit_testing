const cloneArray = require('../utils/testingFunctions').cloneArray;

test('Clones the array', () => {
    const array = [1, 2, 3];
    expect(cloneArray(array)).toEqual(array);
});
