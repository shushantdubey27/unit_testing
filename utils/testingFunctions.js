const add = (a, b) => {
    return a + b;
}

const subtract = (a, b) => {
    return a - b;
}

const multiply = (a, b) => {
    return a * b;
}

const cloneArray = (array) => {
    return [...array];
};

const filterByTerm = (inputArr, searchTerm) => {
    return inputArr.filter(function(arrayElement) {
      return arrayElement.url.match(searchTerm);
    });
  }

module.exports = {
    add,
    subtract,
    multiply,
    cloneArray,
    filterByTerm
};
